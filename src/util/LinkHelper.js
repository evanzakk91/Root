/* eslint-disable */
class LinkHelper {
	static verifyUrl (url) { //Filters out folders
		var rules = /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi; // URL regex rules
		var regex = new RegExp(rules); // Creates regex object
		if ((typeof url !== 'undefined') && (url.match(regex))){
			return true;
		}
		else{
			return false;
		}
	}

    static determineType (bookmark) {
		var rules = /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi; // URL regex rules
		var regex = new RegExp(rules); // Creates regex object

		if (typeof bookmark.url !== 'undefined'){
			if(bookmark.url.match(regex)){
				return 'Link';
			}else{
				return 'Other1';
			}
		}else if(typeof bookmark.children === 'object'){
			return 'Folder';
		}else {
			return 'Other';
		}
	}

	static cleanUrl (url) {
		url      = url.split('://')[1]; //
		url      = url.replace('www.', '');
		url      = url.split('/')[0];
		var name = url.split('.').slice(0, -1);

		return name.join('.');
	}

	static trimTitle (title, length) {
		if (title.length > length){
			return title.substring(0, length-7) + '...'; // Strips string to the first 17 chars and appends '...'
		}
		else{
			return title;
		}
	}

	static createLinkTitle (bookmark, length) {
		var title;
		if(bookmark.title !== ''){
			title = this.trimTitle(bookmark.title, length);
		}else{
			title = this.trimTitle(this.cleanUrl(bookmark.url), length);
		}
		return title;
	}

	static createFileTitle (path, length) {
		var title = 'Unknown';
		if(path.indexOf('/') !== -1){
			title = path.split('/').pop();
		}
		else if(path.indexOf('\\') !== -1){
			title = path.split('\\').pop();
		}
		return this.trimTitle(title, length);
	}
}

export default LinkHelper;
